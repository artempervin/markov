#include "DataProvider.h"
#include <utils.h>

#include <algorithm>
#include <sstream>
#include <boost/algorithm/string.hpp>

void DataProvider::prepare()
{
   m_data.erase (std::remove_if (m_data.begin (), m_data.end (), ::iswpunct), m_data.end ());
   std::transform(m_data.begin(), m_data.end(), m_data.begin(), ::towlower);
   boost::trim(m_data);

   std::wstringstream words;
   std::wistringstream s(m_data);
   std::wstring word;
   while (s >> word)
      words << word << " ";

   m_data = utils::chop(words, 1);
}
