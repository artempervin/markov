#include "MarkovChain.h"

#include <sstream>
#include <fstream>

#include <boost/algorithm/string.hpp>
#include <utils.h>
#include "Prefix.h"

MarkovChain::MarkovChain(const std::wstring& text, size_t sz)
   : m_chain_key_size(sz)
   , m_rnd(m_rnd_dev())
{
   std::vector<std::wstring> tokens;
   boost::split(tokens, text, boost::is_any_of(" "));
   
   Prefix prfx(sz);
   for (const auto& t: tokens)
   {
      if (prfx.size() == sz)
         add(prfx.toString(), t);

      prfx.add(t);
   }
}

MarkovChain::MarkovChain(const char* filename)
   : m_chain_key_size(0)
   , m_rnd(m_rnd_dev())
{
   std::ifstream stream(filename, std::ifstream::in);
   if (!stream)
   {
      int err = errno;

      std::stringstream ss;
      ss << "Failed to open file: " << filename << ": " << strerror(err);
      throw std::logic_error(ss.str());
   }

   while (stream)
   {
      std::string buf;
      std::getline(stream, buf);

      if (!buf.empty())
         parse(utils::mbstowcs(buf.c_str()));
   }

   if (m_chain.size() != 0)
   {
      const auto& e = m_chain.begin()->first;
      m_chain_key_size = std::count(e.begin(), e.end(), ' ')+1;
   }
}

void MarkovChain::dump(std::basic_ostream<wchar_t>& os) const
{
   for (const auto& it: m_chain)
   {
      const auto& k = it.first;
      const auto& v = it.second;

      os << k << ": "; //;  << " [" << v.cumulative_frequency << "]: ";
//      std::cout << k << " [" << v.cumulative_frequency << "]: ";

      std::wstringstream ss;
      for (const auto& e: v.endings)
         ss << e.word << "=" << e.frequency << ", ";
      std::wstring s = utils::chop(ss, 2);

      os << s << std::endl;
   }
}

const std::wstring& MarkovChain::get(const std::wstring& key) const
{
   auto it = m_chain.find(key);
   if (it != m_chain.end())
   {
      const auto& v = it->second;
      const Endings& es = v.endings;

      if (es.size() > 1)
      {
         size_t cum_freq = v.cumulative_frequency;
         
         std::uniform_real_distribution<> dist(0, cum_freq);
         int idx = int(dist(m_rnd))+1;
         
         for (const auto& e: es)
         {
            idx -= e.frequency;
            if (idx <= 0)
               return e.word;
         }
      }
      else
         return es[0].word;
   }

   throw std::runtime_error("Not found!");
}

void MarkovChain::add(const std::wstring& key, const std::wstring& value)
{
   auto it = m_chain.find(key);
   if (it != m_chain.end())
   {
      auto& v = it->second;
      Endings& es = v.endings;
      
      bool found = false;
      for (auto& e: es) 
      {
         if (e.word == value)
         {
            ++e.frequency;
            found = true;
            break;
         }
      }
      
      if (!found)
         v.endings.emplace_back(WordAndFrequency({value, 1}));
      
      ++v.cumulative_frequency;
   }
   else
   {
      Endings e;
      e.emplace_back(WordAndFrequency({value, 1}));
      EndingsWithCumulativeFrequency ew({1, e});
      m_chain.emplace(key, ew);
   }
}

void MarkovChain::parse(const std::wstring& line)
{
   std::vector<std::wstring> tmp;
   boost::split(tmp, line, boost::is_any_of(":"));

   std::wstring k = tmp[0];
   std::wstring v = tmp[1];
   tmp.clear();

   boost::split(tmp, v, boost::is_any_of(","));
         
   Endings es;
   size_t cum_freq = 0;

   for (const auto& e: tmp)
   {
      size_t pos = e.find('=');
      const std::wstring& word = e.substr(1, pos-1);

      size_t freq = stoi(e.substr(pos+1, e.size()));
            
      cum_freq += freq;
      es.emplace_back(word, freq);
   }

   EndingsWithCumulativeFrequency ew({cum_freq, es});
   m_chain.emplace(k, ew);
}
