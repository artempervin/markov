#include <gtest/gtest.h>
#include <stdexcept>
#include <DataProvider.h>

struct TestDataProvider: DataProvider
{
   TestDataProvider(const std::wstring& data)
      : DataProvider(data)
   {}
};

TEST(DataProvider, toLowerTest)
{
   {
      TestDataProvider tdp(L"ABC");
      tdp.prepare();
      ASSERT_EQ(tdp.data(), std::wstring(L"abc"));
   }

   {
      TestDataProvider tdp(L"A b C");
      tdp.prepare();
      ASSERT_EQ(tdp.data(), std::wstring(L"a b c"));
   }

   {
      TestDataProvider tdp(L"а б Ц");
      tdp.prepare();
      ASSERT_EQ(tdp.data(), std::wstring(L"а б ц"));
   }
}

TEST(DataProvider, punctTest)
{
   {
      TestDataProvider tdp(L"a,b!c?");
      tdp.prepare();
      ASSERT_EQ(tdp.data(), std::wstring(L"abc"));
   }

   {
      TestDataProvider tdp(L"!!!");
      tdp.prepare();
      ASSERT_EQ(tdp.data(), std::wstring(L""));
   }

   {
      TestDataProvider tdp(L"а:б'Ц");
      tdp.prepare();
      ASSERT_EQ(tdp.data(), std::wstring(L"абц"));
   }
}

TEST(DataProvider, trimTest)
{
   {
      TestDataProvider tdp(L" abc ");
      tdp.prepare();
      ASSERT_EQ(tdp.data(), std::wstring(L"abc"));
   }

   {
      TestDataProvider tdp(L" абц ");
      tdp.prepare();
      ASSERT_EQ(tdp.data(), std::wstring(L"абц"));
   }
}

int main(int argc, char **argv)
{
   std::setlocale(LC_ALL, "ru_RU.utf8");
   
   testing::InitGoogleTest(&argc, argv);
   return RUN_ALL_TESTS();
}
