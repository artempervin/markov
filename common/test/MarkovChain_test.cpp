#include <gtest/gtest.h>
#include <MarkovChain.h>

struct TestMarkovChain: MarkovChain
{
   typedef MarkovChain Parent;

   void add(const std::wstring& k, const std::wstring& v)
   {
      Parent::add(k, v);
   }

   Chain& chain() 
   {
      return m_chain;
   }
};

TEST(MarkovChain, get)
{
   TestMarkovChain c;
   c.add(L"a", L"b");

   ASSERT_EQ(c.get(L"a"), L"b");
   EXPECT_ANY_THROW({
         c.get(L"b");
   });
}

TEST(MarkovChain, getMultiple)
{
   TestMarkovChain c;
   c.add(L"a", L"b");
   c.add(L"a", L"c");

   bool b_met = false;
   bool c_met = false;

   for (int i = 0; i < 100; ++i)
   {
      const std::wstring& v = c.get(L"a");
      ASSERT_TRUE(v == L"b" || v == L"c");

      if (v == L"b")
         b_met = true;

      if (v == L"c")
         c_met = true;
   }

   ASSERT_TRUE(c_met);
   ASSERT_TRUE(b_met);
}

TEST(MarkovChain, frequencies)
{
   TestMarkovChain c;
   std::wstring k(L"a");
   c.add(k, L"b");  
   ASSERT_EQ(c.chain()[k].cumulative_frequency, 1);
   ASSERT_EQ(c.chain()[k].endings[0].frequency, 1);

   c.add(k, L"b");
   c.add(k, L"c");
   ASSERT_EQ(c.chain()[k].cumulative_frequency, 3);
   const auto& es = c.chain()[k].endings;
   for (const auto& e: es)
   {
      if (e.word == L"b")
         ASSERT_EQ(e.frequency, 2);
      else if (e.word == L"c")
         ASSERT_EQ(e.frequency, 1);
   }
}

int main(int argc, char **argv)
{
   testing::InitGoogleTest(&argc, argv);
   return RUN_ALL_TESTS();
}
