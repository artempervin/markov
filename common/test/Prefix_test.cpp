#include <gtest/gtest.h>
#include <Prefix.h>
#include <stdexcept>

TEST(Prefix, toString)
{
   Prefix prfx(2);
   prfx.add(L"hello");
   prfx.add(L"world");

   ASSERT_EQ(prfx.toString(), L"hello world");
   ASSERT_EQ(prfx.size(), 2);

   prfx.add(L"meh");
   ASSERT_EQ(prfx.toString(), L"world meh");
   ASSERT_EQ(prfx.size(), 2);
}

TEST(Prefix, wrongSize)
{
   EXPECT_ANY_THROW({
         Prefix p(-1);
   });
}

int main(int argc, char **argv)
{
   testing::InitGoogleTest(&argc, argv);
   return RUN_ALL_TESTS();
}
