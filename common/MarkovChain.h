#pragma once

#include <map>
#include <vector>
#include <string>
#include <utility>
#include <random>
#include <unordered_map>
#include "Prefix.h"

struct MarkovChain
{
   MarkovChain(const std::wstring& text, size_t chain_size);
   MarkovChain(const char* filename);

   size_t key_size() const { return m_chain_key_size; }
   void dump(std::basic_ostream<wchar_t>& os) const;

   const std::wstring& get(const std::wstring& key) const;

protected:
   MarkovChain() : m_chain_key_size(0) {}
   void add(const std::wstring& k, const std::wstring& v);
   void parse(const std::wstring& line);

   struct WordAndFrequency
   {
      WordAndFrequency(const std::wstring& word, size_t frequency)
         : word(word)
         , frequency(frequency)
      {}

      std::wstring word;
      size_t frequency;
   };

   typedef std::vector<WordAndFrequency> Endings;
   
   struct EndingsWithCumulativeFrequency
   {
      size_t cumulative_frequency;
      Endings endings;
   };

   typedef std::unordered_map<std::wstring, EndingsWithCumulativeFrequency> Chain;

   Chain m_chain;
   size_t m_chain_key_size;
   std::random_device m_rnd_dev;
   mutable std::mt19937 m_rnd;
};
