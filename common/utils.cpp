#include "utils.h"
#include <memory>

namespace utils
{

std::wstring chop(const std::wstringstream& ss, size_t len = 1)
{
   std::wstring s = ss.str();
   return s.substr(0, s.size() - len);
}

std::wstring mbstowcs(const char* str)
{
   const size_t len = std::mbstowcs(NULL, str, 0);
   std::wstring result;
   result.resize(len);

   if ((size_t)-1 == std::mbstowcs(&result[0], str, len))
      print_error("Failed to convert multibyte characters");

   return result;
}

size_t getLength(const char* str, const char* msg)
{
   int v = atoi(str);
   if (v < 1)
      print_error(msg, ": ", v, "! ", "Must be a natural number!"); 

   return v;
}

}
