#pragma once

#include <iostream>
#include <cstdlib>

template <typename T> 
void print_error(const T& x) 
{
   std::cerr << x << std::endl;
   exit(1);
}

template <typename First, typename... Rest> 
void print_error(const First& first, const Rest&... rest) 
{
   std::cerr << first;
   print_error(rest...); 
}
