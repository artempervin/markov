#pragma once

#include <string>

struct DataProvider
{
   void prepare();

   const std::wstring& data() const { return m_data; }

protected:
   DataProvider() 
   {}

   DataProvider(const std::wstring& data)
      : m_data(data)
   {}

   std::wstring m_data;
};
