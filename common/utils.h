#pragma once

#include <string>
#include <sstream>

#include "print_error.h"

namespace utils
{

std::wstring chop(const std::wstringstream& ss, size_t len);
std::wstring mbstowcs(const char *str);
size_t getLength(const char* str, const char* msg);

}
