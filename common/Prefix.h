#pragma once

#include <boost/circular_buffer.hpp>
#include <boost/algorithm/string.hpp>
#include <utils.h>

struct Prefix
{
   Prefix(size_t sz)
      : m_buffer(sz)
   {}

   void add(const std::wstring& word)
   {
      m_buffer.push_back(word);
   }

   std::wstring toString() const
   {
      const std::wstring& space = L" ";
      return boost::algorithm::join(m_buffer, space);
   }

   size_t size() const 
   { 
      return m_buffer.size(); 
   }
   
private:
   boost::circular_buffer<std::wstring> m_buffer;
};
