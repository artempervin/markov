#pragma once

#include <DataProvider.h>

struct FromStringDataProvider: DataProvider
{
   FromStringDataProvider(const std::wstring& s)
      : DataProvider(s)
   {
      prepare();
   }
};
