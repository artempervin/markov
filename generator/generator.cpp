#include <MarkovChain.h>
#include "FromStringDataProvider.h"

#include <stdexcept>
#include <iostream>

#include <print_error.h>
#include <boost/algorithm/string.hpp>
#include <sstream>
#include <Prefix.h>

void usage(const char* program)
{
   print_error("Usage: ", program, " <FILE_WITH_CHAIN> <LENGTH_OF_SENTENCE_TO_GENERATE> <PHRASE>");
}

int main(int argc, char** argv)
{
   std::setlocale(LC_ALL, "ru_RU.utf8");

   if (argc < 4)
      usage(argv[0]);

   const char* filename = argv[1];
   const size_t length = utils::getLength(argv[2], "Invalid length of sentence to generate");
   
   FromStringDataProvider p(utils::mbstowcs(argv[3]));
   
   try
   {
      MarkovChain c(filename);

      if (c.key_size() >= length)
         print_error("Can't generate the phrase of length: ", length, "! The chain's key size is: ", c.key_size());

      std::vector<std::wstring> words;
      boost::split(words, p.data(), boost::is_any_of(" "));
      size_t curr_len = words.size();

      if (curr_len < c.key_size())
         print_error("Can't generate the phrase: not enough words in the original phrase, need at least: ", c.key_size());

      // build a prefix with last words from the phrase
      Prefix prfx(c.key_size());
      for (size_t i = curr_len - c.key_size(); i < curr_len; ++i)
         prfx.add(words[i]);

      if (curr_len < length)
      {
         std::wcout << p.data() << "...\n...";
         try
         {
            while (curr_len < length)
            {
               const std::wstring& word = c.get(prfx.toString());
               std::wcout << word << " ";
               prfx.add(word);
               ++curr_len;
            }
         }
         catch (const std::exception& ) {} // failed to build the whole phrase, ignore

         std::wcout << "\b." << std::endl;
      }
   }
   catch(const std::exception& e)
   {
      std::cerr << e.what() << std::endl;
   }
}
