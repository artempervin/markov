#include "UrlsFromFileDataProvider.h"

#include <sstream>
#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <utils.h>

#define CURL "curl"
#define CURL_OPTS "-s"

UrlsFromFileDataProvider::UrlsFromFileDataProvider(std::ifstream& stream)
   : m_stream(stream)
{
   fetch();
   prepare();
}

void UrlsFromFileDataProvider::fetch()
{
   std::stringstream cmd;
   cmd << CURL << " " << CURL_OPTS " ";

   while (m_stream)
   {
      std::string line;
      std::getline(m_stream, line);
      if (!line.empty() && line[0] != '#')
         cmd << "--url " << line << " ";
   }

   FILE* pipe = popen(cmd.str().c_str(), "r");
   if (!pipe)
      perror("popen");

   ssize_t len = 0;
   char buffer[4096];
   std::vector<char> data;

   while ((len = fread(buffer, 1, sizeof(buffer), pipe)) > 0) 
      data.insert(data.end(), buffer, buffer + len);
   data.push_back('\0');

   m_data = utils::mbstowcs(&data[0]);

   if (-1 == pclose(pipe))
      perror("pclose");
}
