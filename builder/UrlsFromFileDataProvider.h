#pragma once

#include <fstream>
#include <DataProvider.h>

struct UrlsFromFileDataProvider: DataProvider
{
   UrlsFromFileDataProvider(std::ifstream& stream);   

private:
   void fetch();

   std::ifstream& m_stream;
};
