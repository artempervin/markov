#include <iostream>
#include <fstream>
#include <sstream>
#include <errno.h>
#include <cstring>
#include <vector>
#include <string>

#include <print_error.h>
#include <utils.h>
#include "UrlsFromFileDataProvider.h"
#include <MarkovChain.h>

void usage(const char* program)
{
   print_error("Usage: ", program, " <FILE_WITH_URLS> <LENGTH_OF_CHAINS>");
}

int main(int argc, char** argv)
{
   std::setlocale(LC_ALL, "ru_RU.utf8");

   if (argc < 3)
      usage(argv[0]);

   const char* filename = argv[1];
   const size_t chain_size = utils::getLength(argv[2], "Invalid chain size");
 
   std::ifstream input(filename, std::ifstream::in);
   if (!input)
   {
      int err = errno;

      print_error("Failed to open file ", filename, ": ", strerror(err));
   }

   UrlsFromFileDataProvider p(input);
   std::wstring raw_text = p.data();

   if (!raw_text.empty())
   {
      MarkovChain c(raw_text, chain_size);
      c.dump(std::wcout);
   }
}
