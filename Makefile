include common/common.mk

all: builder/builder generator/generator

builder/builder: builder/*.cpp builder/*.h common/libcommon.so
	make -C builder

generator/generator: generator/*.cpp generator/*.h common/libcommon.so
	make -C generator

common/libcommon.so: common/*.cpp common/*.h
	make -C common

clean: common builder generator
	rm -fr build.log chain.txt
	@for t in $?; do \
	    make -C $$t clean; \
	done;
